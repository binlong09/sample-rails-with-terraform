# frozen_string_literal: true

class ApplicationController < ActionController::API
  def health_check
    render json: { message: 'success' }, status: 200
  end
end
