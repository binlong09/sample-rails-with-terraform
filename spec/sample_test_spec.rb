# frozen_string_literal: true

require 'sample_test'

describe SampleTest do
  describe 'sample test' do
    it 'returns two' do
      expect(1 + 1).to eq(2)
    end
  end
end
