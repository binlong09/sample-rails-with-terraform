variable "prefix" {
  default = "api"
}

variable "project" {
  default = "sample-rails-with-api"
}

variable "contact" {
  default = "email@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "sample-rails-with-api-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "191215300852.dkr.ecr.ap-southeast-1.amazonaws.com/sample-rails-with-terraform"
}

variable "rails_master_key" {
  description = "Rails master key"
}